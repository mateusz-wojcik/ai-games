package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import model.*;

import java.util.ArrayList;
import java.util.List;

public class MainController {

    @FXML
    private Circle white1, white2, white3, white4, white5, white6, white7, white8, white9;

    @FXML
    private Circle black1, black2, black3, black4, black5, black6, black7, black8, black9;

    @FXML
    private AnchorPane gameBoardGroup, paneGameSetup;

    @FXML
    private ScrollPane paneScrollMoves;

    @FXML
    private Label labelMoveColor;

    @FXML
    private RadioButton radioUserVsUser, radioUserVsAI, radioAIvsAI, whiteRadioHeuristic1, whiteRadioHeuristic2, whiteRadioHeuristic3,
            blackRadioHeuristic1, blackRadioHeuristic2, blackRadioHeuristic3, whiteAlgorithmAB, whiteAlgorithmMM, blackAlgorithmMM, blackAlgorithmAB,
            whiteDepth1, whiteDepth2, whiteDepth3, whiteDepth4, whiteDepth5, whiteDepth6,
            blackDepth1, blackDepth2, blackDepth3, blackDepth4, blackDepth5, blackDepth6;

    private List<Figure> whiteFigures, blackFigures;
    private List<Figure> allFigures;
    private MovingManager movingManager;
    private Game game;
    private ToggleGroup toggleGroupMode, toggleGroupHeuristicWhite, toggleGroupHeuristicBlack,
        toggleGroupAlgorithmWhite, toggleGroupAlgorithmBlack, toggleGroupDepthWhite, toggleGroupDepthBlack;

    private Color startingColor = Color.WHITE;

    @FXML
    public void initialize(){
        initFigures();
        setupFigureTooltips();
        setupRadioButtons();
    }

    /*initial setup*/

    private void initFigures(){
        whiteFigures = new ArrayList<>();
        blackFigures = new ArrayList<>();
        allFigures = new ArrayList<>();

        whiteFigures.add(new Figure(white1));
        whiteFigures.add(new Figure(white2));
        whiteFigures.add(new Figure(white3));
        whiteFigures.add(new Figure(white4));
        whiteFigures.add(new Figure(white5));
        whiteFigures.add(new Figure(white6));
        whiteFigures.add(new Figure(white7));
        whiteFigures.add(new Figure(white8));
        whiteFigures.add(new Figure(white9));

        blackFigures.add(new Figure(black1));
        blackFigures.add(new Figure(black2));
        blackFigures.add(new Figure(black3));
        blackFigures.add(new Figure(black4));
        blackFigures.add(new Figure(black5));
        blackFigures.add(new Figure(black6));
        blackFigures.add(new Figure(black7));
        blackFigures.add(new Figure(black8));
        blackFigures.add(new Figure(black9));

        allFigures.addAll(whiteFigures);
        allFigures.addAll(blackFigures);

    }

    public void prepareGame(){
        paneScrollMoves.setContent(new Text());
        movingManager = new MovingManager(gameBoardGroup);
        RadioButton radioButton = (RadioButton) toggleGroupMode.getSelectedToggle();
        RadioButton radioButtonWhiteHeuristic = (RadioButton) toggleGroupHeuristicWhite.getSelectedToggle();
        RadioButton radioButtonBlackHeuristic = (RadioButton) toggleGroupHeuristicBlack.getSelectedToggle();
        RadioButton radioButtonWhiteAlgorithm = (RadioButton) toggleGroupAlgorithmWhite.getSelectedToggle();
        RadioButton radioButtonBlackAlgorithm = (RadioButton) toggleGroupAlgorithmBlack.getSelectedToggle();
        RadioButton radioButtonWhiteDepth = (RadioButton) toggleGroupDepthWhite.getSelectedToggle();
        RadioButton radioButtonBlackDepth = (RadioButton) toggleGroupDepthBlack.getSelectedToggle();
        switch (radioButton.getId()) {
            case "radioUserVsUser":
                game = new GameUser(whiteFigures, blackFigures, movingManager, startingColor);
                break;
            case "radioUserVsAI":
                game = new GameUservsAI(whiteFigures, blackFigures, movingManager, startingColor, radioButtonWhiteHeuristic.getId(),
                        radioButtonWhiteAlgorithm.getId(), Integer.parseInt(radioButtonWhiteDepth.getId().substring(radioButtonWhiteDepth.getId().length() - 1)));
                break;
            case "radioAIvsAI":
                game = new GameAIvsAI(whiteFigures, blackFigures, movingManager, startingColor, radioButtonWhiteHeuristic.getId(),
                        radioButtonBlackHeuristic.getId(), radioButtonWhiteAlgorithm.getId(), radioButtonBlackAlgorithm.getId(),
                        Integer.parseInt(radioButtonWhiteDepth.getId().substring(radioButtonWhiteDepth.getId().length() - 1)),
                        Integer.parseInt(radioButtonBlackDepth.getId().substring(radioButtonBlackDepth.getId().length() - 1)));
                break;
        }
        game.sendControlsToUpdate(List.of(labelMoveColor, paneScrollMoves));
    }

    /*onclicks*/

    public void onStartClick(){
        if (game != null) game.reset();
        prepareGame();
        game.start();
    }

    public void onResetClick(){
        game.reset();
    }

    public void onGameOptionsClick(){
        paneScrollMoves.setVisible(false);
        paneGameSetup.setVisible(true);
    }

    public void onMovesLogClick(){
        paneScrollMoves.setVisible(true);
        paneGameSetup.setVisible(false);
    }

    /*interface*/

    private void setupRadioButtons(){
        toggleGroupMode = new ToggleGroup();
        radioUserVsUser.setToggleGroup(toggleGroupMode);
        radioUserVsAI.setToggleGroup(toggleGroupMode);
        radioAIvsAI.setToggleGroup(toggleGroupMode);

        toggleGroupAlgorithmWhite = new ToggleGroup();
        whiteAlgorithmAB.setToggleGroup(toggleGroupAlgorithmWhite);
        whiteAlgorithmMM.setToggleGroup(toggleGroupAlgorithmWhite);

        toggleGroupAlgorithmBlack = new ToggleGroup();
        blackAlgorithmAB.setToggleGroup(toggleGroupAlgorithmBlack);
        blackAlgorithmMM.setToggleGroup(toggleGroupAlgorithmBlack);

        toggleGroupHeuristicWhite = new ToggleGroup();
        whiteRadioHeuristic1.setToggleGroup(toggleGroupHeuristicWhite);
        whiteRadioHeuristic2.setToggleGroup(toggleGroupHeuristicWhite);
        whiteRadioHeuristic3.setToggleGroup(toggleGroupHeuristicWhite);

        toggleGroupHeuristicBlack = new ToggleGroup();
        blackRadioHeuristic1.setToggleGroup(toggleGroupHeuristicBlack);
        blackRadioHeuristic2.setToggleGroup(toggleGroupHeuristicBlack);
        blackRadioHeuristic3.setToggleGroup(toggleGroupHeuristicBlack);

        toggleGroupDepthWhite = new ToggleGroup();
        whiteDepth1.setToggleGroup(toggleGroupDepthWhite);
        whiteDepth2.setToggleGroup(toggleGroupDepthWhite);
        whiteDepth3.setToggleGroup(toggleGroupDepthWhite);
        whiteDepth4.setToggleGroup(toggleGroupDepthWhite);
        whiteDepth5.setToggleGroup(toggleGroupDepthWhite);
        whiteDepth6.setToggleGroup(toggleGroupDepthWhite);

        toggleGroupDepthBlack = new ToggleGroup();
        blackDepth1.setToggleGroup(toggleGroupDepthBlack);
        blackDepth2.setToggleGroup(toggleGroupDepthBlack);
        blackDepth3.setToggleGroup(toggleGroupDepthBlack);
        blackDepth4.setToggleGroup(toggleGroupDepthBlack);
        blackDepth5.setToggleGroup(toggleGroupDepthBlack);
        blackDepth6.setToggleGroup(toggleGroupDepthBlack);

    }

    private void setupFigureTooltips(){
        allFigures.forEach(figure -> setTooltipText(figure.getColor().toString() + " " + figure.getCircle().getId().substring(figure.getCircle().getId().length() - 1), figure));
    }

    public void setTooltipText(String text, Figure figure){
        Tooltip tooltip = new Tooltip(text);
        Tooltip.install(figure.getCircle(), tooltip);
    }

}
