import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String args[]){
        launch();
    }

    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("layout/main_stage.fxml"));

        Scene scene = new Scene(root, 1400.0, 840.0);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
}
