package model;

import java.util.Arrays;

public class Counter {

    public static Color currentColor;

    public static int instructionsWhite;
    public static int instructionsBlack;
    public static int movesWhite;
    public static int movesBlack;
    public static long timeWhite;
    public static long timeBlack;

    public static int currentMoveInstructionsWhite;
    public static int currentMoveInstructionsBlack;

    public static String moveInstructionsWhite = "";
    public static String moveInstructionsBlack = "";

    public static void printResults() {
        try {
            //System.out.println("White instructions: " + instructionsWhite);
            System.out.println("White moves: " + movesWhite);
            System.out.println("White time: " + (timeWhite / 1e9) + " [s]");
            System.out.println(moveInstructionsWhite);
            System.out.println("White instructions: " + Arrays.stream(moveInstructionsWhite.trim().split(" ")).mapToInt(Integer::parseInt).sum());
            //System.out.println("Black instructions: " + instructionsBlack);
            System.out.println("Black moves: " + movesBlack);
            System.out.println("Black time: " + (timeBlack / 1e9) + " [s]");
            System.out.println(moveInstructionsBlack);
            System.out.println("Black instructions: " + Arrays.stream(moveInstructionsBlack.trim().split(" ")).mapToInt(Integer::parseInt).sum());
        } catch (Exception ex) {}
    }
}
