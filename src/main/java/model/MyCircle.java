package model;

import javafx.scene.shape.Circle;

import java.io.Serializable;

public class MyCircle extends Circle implements Serializable {

    public MyCircle(Circle circle){
        this.setId(circle.getId());
    }
}
