package model;

public class GameState {

    private Game game;

    public GameState(Game game){
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public GameState copy(){
        return new GameState(game.copy());
    }
}
