package model;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Game {
    List<Figure> whiteFigures;
    List<Figure> blackFigures;
    List<Figure> allFigures;
    List<Node> intefaceControls;
    HashMap<String, String> fields;
    Color color;
    MovingManager movingManager;
    HashSet<String> lastWhiteMill, lastBlackMill;
    int gameStage;
    int movesWithoutMill;
    String winner;
    int blackEngaged, whiteEngaged;
    int blackKilled, whiteKilled;
    int gameStageWhite = 1;
    int gameStageBlack = 1;
    String lastMovedWhite, lastMovedBlack;
    String heuristicWhite, heuristicBlack, algorithmWhite, algorithmBlack;
    int depthWhite, depthBlack;

    public abstract void start();

    public void destroy() {
        allFigures.forEach(Figure::reset);
    }

    public void reset() {
        allFigures.forEach(Figure::reset);
        disableFigures(Color.WHITE);
        disableFigures(Color.BLACK);
        setMoveColorLabel(null);
        appendMoveToScrollPane(null);
        allFigures.forEach(fig -> {
            fig.getCircle().setOnMouseClicked(null);
        });
    }

    public void performAction(Figure figure) {
    }

    public void performAction(String action, boolean isInterface) {
    }

    public void performAction() {
    }

    public boolean performMove(Figure figure) {
        return false;
    }

    public boolean performMove(String move, boolean isInterface) {
        return false;
    }

    public void afterMove(Figure figure) {
    }

    public void afterMove() {
    }

    public void removeFigure() {
    }

    public void sendControlsToUpdate(List<Node> nodes) {
        this.intefaceControls = nodes;
    }

    public abstract Game copy();

    public void finish() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game over");
        alert.setHeaderText(winner.equals("DRAW") ? winner : winner + " wins");
        alert.showAndWait();
    }

    public void changePlayer() {
        disableFigures(color);
        changeColor();
        enableFigures(color);
        setMoveColorLabel(color);
    }

    public int countMills() {
        int mills = 0;
        for (HashSet<String> millConfiguration : MovingManager.getMillConfigurations()) {
            if (isMill(millConfiguration)){
                mills++;
                if (color == Color.WHITE) {
                    lastWhiteMill = millConfiguration;
                } else {
                    lastBlackMill = millConfiguration;
                }
                movesWithoutMill = 0;
            }
        }
        if (mills == 0) movesWithoutMill++;
        return mills;
    }

    public boolean isMill(HashSet<String> fieldsToCheck) {
        //System.out.println("----");
        if (!fieldsToCheck.contains(this.color == Color.WHITE ? lastMovedWhite : lastMovedBlack)
            || fieldsToCheck.equals(this.color == Color.WHITE ? lastWhiteMill  : lastBlackMill)) {
            return false;
        }
        for (String field : fieldsToCheck) {
            //TODO speed
            if (!(this.fields.get(field) != null && this.fields.get(field).toLowerCase().startsWith(this.color.toString().substring(0, 1).toLowerCase()))) {
                return false;
            }

          //  System.out.println(this.fields.get(field));
        }
        //System.out.println("Mill " + fieldsToCheck);
        return true;
    }

    public boolean isMill(Figure figure) {
        List<String> fields = getOccupiedFields(color);
        List<String> configurations = MovingManager.getCmillConfigurations().get(figure.getCurrentPosition());

        String[] millArray = configurations
                .stream()
                .map(conf -> conf.split("-"))
                .filter(conf -> fields.contains(conf[0]) && fields.contains(conf[1]) && fields.contains(conf[2]))
                .findFirst()
                .orElse(null);

        if (millArray != null) {
            String mill = String.join("-", millArray);

//            if (color == Color.WHITE) {
//                lastWhiteMill = mill;
//            } else {
//                lastBlackMill = mill;
//            }
            movesWithoutMill = 0;
            return true;
        }
        movesWithoutMill++;
        return false;
    }

    public List<Figure> getAliveFigures(Color color) {
        List<Figure> figures = color == Color.WHITE ? whiteFigures : blackFigures;
        return figures.stream()
                .filter(figure -> !figure.getCurrentPosition().equals("START") && !figure.getCurrentPosition().equals("END"))
                .collect(Collectors.toList());
    }

    //TODO optimize
    public List<Figure> getAliveFiguresSimulation(Color color) {
        List<Figure> figures = color == Color.WHITE ? whiteFigures : blackFigures;
        return figures.stream()
                .filter(figure -> this.fields.containsValue(figure.getCircle().getId()))
                .collect(Collectors.toList());
    }



    public List<String> getEmptyFields() {
        return movingManager.getMoveCoordinates().keySet()
                .stream()
                .filter(key -> allFigures
                        .stream()
                        .noneMatch(figure -> !figure.getCurrentPosition().equals("START")
                                && !figure.getCurrentPosition().equals("END")
                                && !figure.isDead()
                                && figure.getCurrentPosition().equals(key))).collect(Collectors.toList());
    }

    public List<String> getEmptyFieldsSimulation(){
        return fields.keySet().stream().filter(field -> this.fields.get(field) == null).collect(Collectors.toList());
    }

    public List<String> getOccupiedFields(Color color) {
        List<Figure> figuresToCheck = color == null ? allFigures : (color == Color.WHITE ? whiteFigures : blackFigures);
        return figuresToCheck
                .stream()
                .filter(figure -> !figure.getCurrentPosition().equals("START")
                        && !figure.getCurrentPosition().equals("END"))
                .map(Figure::getCurrentPosition)
                .collect(Collectors.toList());
    }

    public List<String> getPossibleMoves(Color color) {
        List<String> possibleMoves = new ArrayList<>();
        List<Figure> figures = color == Color.WHITE ? whiteFigures : blackFigures;
        List<String> emptyFields = getEmptyFields();

        //System.out.println("Possible figures: " + figures.toString());

        if (isFirstGameStage()) {
            //if figure is outisde board - every move possible
            figures.stream()
                    .filter(figure -> figure.getCurrentPosition().equals("START"))
                    .forEach(figure -> getEmptyFields()
                            .forEach(field -> possibleMoves.add(figure.getCircle().getId() + "-" + field)));
            figures.stream()
                    .filter(figure -> !figure.getCurrentPosition().equals("START"))
                    .forEach(Figure::disableMove);
            //System.out.println("True");
        } else {
            //get only available figures on board
            figures = figures.stream().filter(figure -> !figure.getCurrentPosition().equals("START")
                    && !figure.getCurrentPosition().equals("END")).collect(Collectors.toList());

            //if figure is on board - get empty neighbours
            if (figures.size() > 3) {
                gameStage = 2;
                figures.stream()
                        .filter(figure -> !figure.getCurrentPosition().equals("START")
                                && !figure.getCurrentPosition().equals("END"))
                        .forEach(figure -> movingManager.getNeighbours().get(figure.getCurrentPosition())
                                .stream()
                                .filter(emptyFields::contains)
                                .forEach(field -> possibleMoves.add(figure.getCurrentPosition() + "-" + field)));
                figures.stream()
                        .filter(figure -> figure.getCurrentPosition().equals("START")
                                || figure.getCurrentPosition().equals("END"))
                        .forEach(Figure::disableMove);
            }
            //if less than 3 figures on board - every empty field is possible to move
            else {
                gameStage = 3;
                figures.stream()
                        .filter(figure -> !figure.getCurrentPosition().equals("START")
                                && !figure.getCurrentPosition().equals("END"))
                        .forEach(figure -> getEmptyFields()
                                .forEach(field -> possibleMoves.add(figure.getCurrentPosition() + "-" + field)));
                figures.stream()
                        .filter(figure -> figure.getCurrentPosition().equals("START")
                                || figure.getCurrentPosition().equals("END"))
                        .forEach(Figure::disableMove);
            }
        }

        //System.out.println(possibleMoves.toString());
        return possibleMoves;
    }

    public boolean isGameEnd() {
        if (movesWithoutMill >= 50) {
            winner = "DRAW";
            return true;
        } else if (whiteKilled > 6) {
            winner = Color.BLACK.toString();
            return true;
        } else if (blackKilled > 6) {
            winner = Color.WHITE.toString();
            return true;
        }
        return false;
    }

    public boolean isFirstGameStage() {
        return allFigures.stream().anyMatch(figure -> figure.getCurrentPosition().equals("START"));
    }

    public void removeFromBoard(Figure figure) {
        figure.kill();
        this.fields.put(getCurrentFigurePosition(this.fields, figure.getCircle().getId()), null);
        this.allFigures.remove(figure);
        this.whiteFigures.remove(figure);
        this.blackFigures.remove(figure);
    }

    public void removeFigureFromBoard(String fieldKey) {
        allFigures
                .stream()
                .filter(figure -> figure.getCurrentPosition().equals(fieldKey))
                .findFirst().ifPresent(Figure::kill);
    }

    public void disableFigures(Color color) {
        if (color == Color.WHITE) whiteFigures.forEach(Figure::disableMove);
        else blackFigures.forEach(Figure::disableMove);
    }

    public void enableFigures(Color color) {
        if (color == Color.WHITE) whiteFigures.forEach(Figure::enableMove);
        else blackFigures.forEach(Figure::enableMove);
    }

    public void showFigures() {
        allFigures.forEach(figure -> figure.getCircle().setVisible(true));
    }

    public void alignToClosestField(Figure figure) {
        if (MovingManager.isOnBoard(figure)) {
            figure.moveTo(MovingManager.getColsestCoordinate(figure.getCoordinate()));
            System.out.println(figure.getPositionX() + " " + figure.getPositionY());
        }
    }

    public void changeColor() {
        this.color = this.color == Color.WHITE ? Color.BLACK : Color.WHITE;
    }

    public void setMoveColorLabel(Color color) {
        String text = color == null ? "" : color + " turn";
        try {
            ((Label) intefaceControls.get(0)).setText(text);
        } catch (NullPointerException ignored) {

        }
    }

    public void appendMoveToScrollPane(String move) {
        ScrollPane scrollPane = (ScrollPane) intefaceControls.get(1);
        if (move != null) {
            Text text = (Text) scrollPane.getContent();
            text.setText(text.getText() + move + "\n");
            scrollPane.setContent(text);
        } else {
            scrollPane.setContent(new Text());
        }
    }

    public Figure getFigureByFieldKey(String fieldKey) {
        return allFigures.stream().filter(figure -> figure.getCircle().getId().equals(this.fields.get(fieldKey))).findFirst().orElse(null);
    }

    public Figure getFigureById(String id) {
        return allFigures.stream().filter(figure -> figure.getCircle().getId().equals(id)).findFirst().orElse(null);
    }

    public void performMoveFromString(String from, String to) {
        Figure figureToMove = from.length() > 2 ? getFigureById(from) : getFigureByFieldKey(from);
        figureToMove.moveTo(to);
    }

    public void performMoveFromStringNoInterface(String from, String to) {
        if (from.length() > 2) {
            this.fields.put(to, from);
        } else {
            this.fields.put(to, this.fields.get(from));
            this.fields.put(from, null);
        }
    }

    public String getCurrentFigurePosition(HashMap<String, String> hashMap, String value){
        return hashMap.keySet().stream().filter(key -> hashMap.get(key) != null && hashMap.get(key).equals(value)).findFirst().orElse(null);
    }

    void enableInteface() {
        allFigures.forEach(figure -> figure.setInterfaceEnabled(true));
    }
}
