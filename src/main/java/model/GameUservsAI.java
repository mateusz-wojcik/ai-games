package model;

import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.util.*;
import java.util.stream.Collectors;

public class GameUservsAI extends Game {

    boolean canMove = true;

    public GameUservsAI(List<Figure> whiteFigures, List<Figure> blackFigures, MovingManager movingManager, Color color, String heuristicWhite,
                      String algorithmWhite, int depthWhite) {
        this.whiteFigures = whiteFigures;
        this.blackFigures = blackFigures;
        this.allFigures = new ArrayList<>();
        this.allFigures.addAll(whiteFigures);
        this.allFigures.addAll(blackFigures);
        this.color = color;
        this.movingManager = movingManager;
        this.fields = MovingManager.getFields();
        this.lastWhiteMill = new HashSet<>();
        this.lastBlackMill = new HashSet<>();
        this.heuristicWhite = heuristicWhite;
        this.algorithmWhite = algorithmWhite;
        this.depthWhite = depthWhite;
    }

    @Override
    public void start() {
        System.out.println("Started game --------------");

        gameStage = 1;
        movesWithoutMill = 0;
        enableFigures(Utils.otherColor(color));
        setupCircleDrag();
        setMoveColorLabel(color);
        enableInteface();
        performAction();
    }

    @Override
    public void finish() {
        System.out.println("Winner: " + winner);
        Counter.printResults();
        System.out.println("End game --------------");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game over");
        alert.setHeaderText(winner.equals("DRAW") ? winner : winner + " wins");
        alert.showAndWait();
    }

    public boolean performActionUser(Figure figure) {
        List<String> possibleMoves = getPossibleMoves(color);

        if (possibleMoves.isEmpty()){
            winner = Utils.otherColor(color).toString();
            finish();
        }

        if (MovingManager.isOnBoard(figure)) {
            Coordinate destination = MovingManager.getColsestCoordinate(figure.getCoordinate());
            if (Objects.requireNonNull(MovingManager.getFieldKeyFromCoordinate(destination)).equals(figure.getCurrentPosition())) {
                figure.backToDragStart();
                return false;
            }

            String moveStage1 = figure.getCircle().getId() + "-" + MovingManager.getFieldKeyFromCoordinate(destination);
            String moveOtherStage = figure.getCurrentPosition() + "-" + MovingManager.getFieldKeyFromCoordinate(destination);

            if (possibleMoves.contains(moveStage1)) {
                figure.moveTo(destination);
                appendMoveToScrollPane(moveStage1);

                performMove(moveStage1, true);
                afterMove();

            } else if (possibleMoves.contains(moveOtherStage)) {
                figure.moveTo(destination);
                appendMoveToScrollPane(moveOtherStage);

                performMove(moveOtherStage, true);
                afterMove();

            } else {
                figure.backToDragStart();
                return false;
            }
        } else {
            figure.backToDragStart();
            return false;
        }

        //AI move
        if (isGameEnd()) {
            finish();
        } else {
            if (canMove) {
                changePlayer();
                Platform.runLater(this::performAction);
            }
        }
        //TODO update game state
        return true;
    }

    @Override
    public void performAction() {
        long start = System.nanoTime();
//        try {
//            Thread.sleep(200);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        //update game state
        List<String> possibleMoves = getPossibleMoves(color);

//        if (possibleMoves.isEmpty()){
//            winner = Utils.otherColor(color).toString();
//            finish();
//        }

        String moveToPerform = getBestMove();

        if (performMove(moveToPerform, true)) {
            afterMove();

            long stop = System.nanoTime();

            if (this.color == Color.WHITE) {
                Counter.timeWhite += stop - start;
                Counter.movesWhite++;
            } else {
                Counter.timeBlack += stop - start;
                Counter.movesBlack++;
            }

            if (isGameEnd()) {
                finish();
            } else {
                changePlayer();
            }
        }
    }

    @Override
    public void performAction(String move, boolean isInterface) {
        if (performMove(move, isInterface)) {
            //TODO game ends?
            afterMove();
            changePlayer();
        }
    }

    @Override
    public boolean performMove(String move, boolean isInterface) {
        String[] fromTo = move.split("-");
        if (isInterface) {
            performMoveFromString(fromTo[0], fromTo[1]);
            appendMoveToScrollPane(move);
        }
        performMoveFromStringNoInterface(fromTo[0], fromTo[1]);

        if (this.color == Color.WHITE) lastMovedWhite = fromTo[1];
        else lastMovedBlack = fromTo[1];

        return true;
    }

    @Override
    public void afterMove() {
        int millsCount = countMills();
        System.out.println(millsCount);
        if (millsCount > 0) {
            if (color == Color.WHITE) {
                removeFigure();
                if (millsCount == 2) removeFigure();
            } else {
                removeFigureUser();
                if (millsCount == 2) removeFigureUser();
            }
        }
        if (canMove) updateGameState();
    }

    //TODO
    @Override
    public void removeFigure() {
        Color colorToRemove = Utils.otherColor(this.color);
        List<Figure> figuresOnBoard = getAliveFiguresSimulation(colorToRemove);

        removeFromBoard(figuresOnBoard.get(0));

        if (colorToRemove == Color.WHITE) {
            whiteKilled = gameStageWhite == 1 ? ++whiteKilled : whiteKilled;
        }
        else blackKilled = gameStageBlack == 1 ? ++blackKilled : blackKilled;

    }

    public void removeFigureUser(){
        canMove = false;

        Color colorToRemove = Utils.otherColor(this.color);

        getAliveFigures(colorToRemove).forEach(figure -> figure.getCircle().setOnMouseClicked(e -> {
            removeFromBoard(figure);

            if (colorToRemove == Color.WHITE) {
                whiteKilled = gameStageWhite == 1 ? ++whiteKilled : whiteKilled;
            }
            else blackKilled = gameStageBlack == 1 ? ++blackKilled : blackKilled;

            getAliveFigures(colorToRemove).forEach(fig -> {
                fig.getCircle().setOnMouseClicked(null);
            });

            canMove = true;

            updateGameState();

            if (isGameEnd()) {
                finish();
            } else {
                changePlayer();
                Platform.runLater(this::performAction);
            }

        }));

    }

    @Override
    public void changePlayer() {
        changeColor();
        setMoveColorLabel(color);
    }

    @Override
    public Game copy() {
        List<Figure> whiteFigures = this.whiteFigures.stream()
                .map(Figure::new)
                .collect(Collectors.toList());
        List<Figure> blackFigures = this.blackFigures.stream()
                .map(Figure::new)
                .collect(Collectors.toList());
        Color color = this.color;
        Game game = new GameUservsAI(whiteFigures, blackFigures, movingManager, color, heuristicWhite,
                algorithmWhite, depthWhite);
        game.lastWhiteMill = lastWhiteMill;
        game.lastBlackMill = lastBlackMill;
        game.gameStage = gameStage;
        game.movesWithoutMill = movesWithoutMill;
        game.winner = winner;
        game.gameStageBlack = gameStageBlack;
        game.gameStageWhite = gameStageWhite;
        game.blackEngaged = blackEngaged;
        game.blackKilled = blackKilled;
        game.whiteEngaged = whiteEngaged;
        game.whiteKilled = whiteKilled;
        game.fields = new HashMap<>();
        game.fields.putAll(this.fields);
        game.lastWhiteMill = new HashSet<>(lastWhiteMill);
        game.lastBlackMill = new HashSet<>(lastBlackMill);
        return game;
    }

    public void updateGameState() {
        if (color == Color.WHITE && whiteEngaged < 9) whiteEngaged++;
        else if (color == Color.BLACK && blackEngaged < 9) blackEngaged++;

        if (gameStageWhite == 1 && whiteEngaged == 9) {
            gameStageWhite = 2;
        }
        if (gameStageBlack == 1 && blackEngaged == 9) {
            gameStageBlack = 2;
        }
        if (gameStageWhite == 2 && whiteKilled > 5) gameStageWhite = 3;
        if (gameStageBlack == 2 && blackKilled > 5) gameStageBlack = 3;

        Color colorToRemove = Utils.otherColor(this.color);
        List<Figure> figuresOnBoard = getAliveFiguresSimulation(colorToRemove);
        if (colorToRemove == Color.WHITE) {
            whiteKilled = gameStageWhite == 1 ? whiteKilled : 9 - figuresOnBoard.size();
        }
        else blackKilled = gameStageBlack == 1 ? blackKilled : 9 - figuresOnBoard.size();

    }

    //TODO
    public String getBestMove() {
        GameSimulation gameSimulation = new GameSimulation(new GameState(this.copy()), algorithmWhite, depthWhite);
        return gameSimulation.getBestMove();

//        List<String> possibleMoves = getPossibleMoves(color);
//        return possibleMoves.get(new Random().nextInt(possibleMoves.size()));
        //return null;
    }

    @Override
    public List<String> getPossibleMoves(Color color) {
        List<String> possibleMoves = new ArrayList<>();
        List<Figure> figures = color == Color.WHITE ? whiteFigures : blackFigures;
        List<String> emptyFields = getEmptyFields();
        //System.out.println("Possible figures: " + figures.toString());

        int currentColorStage = color == Color.WHITE ? gameStageWhite : gameStageBlack;

        if (currentColorStage == 1) {
            //if figure is outisde board - every move possible
            Figure fig = figures.stream()
                    .filter(figure -> !this.fields.containsValue(figure.getCircle().getId()))
                    .findFirst()
                    .orElse(null);

            if (fig != null) {
                for (String field : getEmptyFieldsSimulation()) {
                    possibleMoves.add(fig.getCircle().getId() + "-" + field);
                }
            }

        } else if (currentColorStage == 2) {
            //get only available figures on board

            //if figure is on board - get empty neighbours
            figures.stream()
                    .filter(figure -> this.fields.containsValue(figure.getCircle().getId()))
                    .forEach(figure -> movingManager.getNeighbours().get(getCurrentFigurePosition(this.fields, figure.getCircle().getId()))
                            .stream()
                            .filter(emptyFields::contains)
                            .forEach(field -> possibleMoves.add(getCurrentFigurePosition(this.fields, figure.getCircle().getId()) + "-" + field)));
        }
        //if less than 3 figures on board - every empty field is possible to move
        else {
            figures.stream()
                    .filter(figure -> this.fields.containsValue(figure.getCircle().getId()))
                    .forEach(figure -> getEmptyFields()
                            .forEach(field -> possibleMoves.add(getCurrentFigurePosition(this.fields, figure.getCircle().getId()) + "-" + field)));
        }
        return possibleMoves;
    }

    private void setupCircleDrag() {

        allFigures.forEach(fig -> fig.getCircle().setOnDragDetected(mouseEvent -> {
            fig.getCircle().startFullDrag();
        }));

        allFigures.forEach(fig -> fig.getCircle().setOnMouseDragReleased(mouseDragEvent -> {
            performActionUser(fig);
        }));

    }

}
