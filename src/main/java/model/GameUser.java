package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GameUser extends Game {

    public GameUser(List<Figure> whiteFigures, List<Figure> blackFigures, MovingManager movingManager, Color color) {
        this.whiteFigures = whiteFigures;
        this.blackFigures = blackFigures;
        this.allFigures = new ArrayList<>();
        this.allFigures.addAll(whiteFigures);
        this.allFigures.addAll(blackFigures);
        this.color = color;
        this.movingManager = movingManager;
    }

    @Override
    public void start() {
        gameStage = 1;
        movesWithoutMill = 0;
        setupCircleDrag();
        enableFigures(color);
        setMoveColorLabel(color);
        enableInteface();
    }

    @Override
    public void performAction(Figure figure) {
        System.out.println("Game stage: " + gameStage);
        if (performMove(figure)) {
            afterMove(figure);
            if (isGameEnd()) {
                finish();
            }
            changePlayer();
        }
    }

    @Override
    public boolean performMove(Figure figure) {
        if (MovingManager.isOnBoard(figure)) {
            Coordinate destination = MovingManager.getColsestCoordinate(figure.getCoordinate());
            if (Objects.requireNonNull(MovingManager.getFieldKeyFromCoordinate(destination)).equals(figure.getCurrentPosition())) {
                figure.backToDragStart();
                return false;
            }
            if (getPossibleMoves(color).contains(figure.getCircle().getId() + "-" + MovingManager.getFieldKeyFromCoordinate(destination))) {
                figure.moveTo(destination);
                appendMoveToScrollPane(figure.getCircle().getId() + "-" + MovingManager.getFieldKeyFromCoordinate(destination));

            } else if (getPossibleMoves(color).contains(figure.getCurrentPosition() + "-" + MovingManager.getFieldKeyFromCoordinate(destination))) {
                figure.moveTo(destination);
                appendMoveToScrollPane(figure.getCurrentPosition() + "-" + MovingManager.getFieldKeyFromCoordinate(destination));

            } else {
                figure.backToDragStart();
                return false;
            }
        } else {
            figure.backToDragStart();
            return false;
        }
        //TODO update game state
        return true;
    }

    @Override
    public void afterMove(Figure figure) {
        if (isMill(figure)) {
            System.out.println("Mill present! " + (color == Color.WHITE ? lastWhiteMill : lastBlackMill));
            //TODO
            //removeFromBoard(getAliveFigures(Utils.otherColor(color)).get(0));
            removeFigure();
            movesWithoutMill = 0;
        } else {
            movesWithoutMill++;
        }
    }

    @Override
    public void removeFigure(){
        Color colorToRemove = Utils.otherColor(this.color);

        getAliveFigures(colorToRemove).forEach(figure -> figure.getCircle().setOnMouseClicked(e -> {
            removeFromBoard(figure);
            getAliveFigures(colorToRemove).forEach(fig -> {
                fig.getCircle().setOnMouseClicked(null);
                System.out.println(fig.getCircle().getId() + " event removed");
            });
        }));

    }

    @Override
    public Game copy() {
        return null;
    }

    private void setupCircleDrag() {

        allFigures.forEach(fig -> fig.getCircle().setOnDragDetected(mouseEvent -> {
            fig.getCircle().startFullDrag();
        }));

        allFigures.forEach(fig -> fig.getCircle().setOnMouseDragReleased(mouseDragEvent -> {
            performAction(fig);
        }));

    }

}
