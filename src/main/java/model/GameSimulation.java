package model;

import java.util.ArrayList;
import java.util.List;

public class GameSimulation {

    private GameState gameState;
    private String algorithm;
    private int maxDepth;
    private Heuristic heuristicWhite;
    private Heuristic heuristicBlack;
    private boolean sortFirst;
    private boolean sortAll;

    public GameSimulation(GameState gameState, String algorithm, int maxDepth) {
        this.gameState = gameState;
        this.maxDepth = maxDepth;
        this.algorithm = algorithm;
        this.heuristicWhite = getHeuristicFromString(gameState.getGame().heuristicWhite);
        this.heuristicBlack = getHeuristicFromString(gameState.getGame().heuristicBlack) == null ? this.heuristicWhite : getHeuristicFromString(gameState.getGame().heuristicBlack);
        this.sortFirst = false;
        this.sortAll = false;
    }

    public Heuristic getHeuristicFromString(String info) {
        if (info == null) return null;

        switch (info.substring(info.length() - 1)) {
            case "1":
                return new HeuristicMovesCount();
            case "2":
                return new HeuristicFiguresCount();
            case "3":
                return new HeuristicMovesAndFiguresCount();
        }

        return null;
    }

    public String getBestMove() {
        return algorithm.contains("AB") ? getBestMoveAlphaBeta() : getBestMoveMinMax();
    }

    public String getBestMoveMinMax() {
        float bestResult = -1;
        String bestMove = null;
        List<String> possibleMoves = gameState.getGame().getPossibleMoves(gameState.getGame().color);
        for (String move : possibleMoves) {
            float result = minmax(move, 0, gameState.copy());
            if (result > bestResult || bestResult == -1) {
                bestResult = result;
                bestMove = move;
            }
        }
        return bestMove;
    }

    public float minmax(String move, int depth, GameState gameState) {
        gameState.getGame().performAction(move, false);
        if (depth >= maxDepth || gameState.getGame().isGameEnd()) {
            return gameState.getGame().color == Color.WHITE ? heuristicWhite.evaluate(gameState) : heuristicBlack.evaluate(gameState);
        }
        float bestResult = -1;
        List<String> possibleMoves = gameState.getGame().getPossibleMoves(gameState.getGame().color);
        for (String nextMove : possibleMoves) {
            float result = minmax(nextMove, depth + 1, gameState.copy());
            if (bestResult == -1) bestResult = result;
            bestResult = gameState.getGame().color == Color.WHITE ? Math.max(bestResult, result) : Math.min(bestResult, result);
        }
        return bestResult;
    }

    public String getBestMoveAlphaBeta() {
        Counter.currentColor = gameState.getGame().color;

        if (Counter.currentColor == Color.WHITE) {
            Counter.currentMoveInstructionsWhite = Counter.instructionsWhite;
        } else {
            Counter.currentMoveInstructionsBlack = Counter.instructionsBlack;
        }

        float bestResult = -Float.MAX_VALUE;
        String bestMove = null;

        List<String> possibleMoves = gameState.getGame().getPossibleMoves(gameState.getGame().color);

        if (sortFirst) sortMoves(possibleMoves, gameState, gameState.getGame().color == Color.WHITE);

        for (String move : possibleMoves) {
            float result = alphabeta(move, 0, gameState.copy(), -Float.MAX_VALUE, Float.MAX_VALUE);
            if (result > bestResult) {
                bestResult = result;
                bestMove = move;
            }
        }

        if (Counter.currentColor == Color.WHITE) {
            Counter.moveInstructionsWhite += Counter.instructionsWhite - Counter.currentMoveInstructionsWhite + " ";
        } else {
            Counter.moveInstructionsBlack += Counter.instructionsBlack - Counter.currentMoveInstructionsBlack + " ";
        }

        return bestMove;
    }

    public float alphabeta(String move, int depth, GameState gameState, float alpha, float beta) {
        if (Counter.currentColor == Color.WHITE) {
            Counter.instructionsWhite++;
        } else {
            Counter.instructionsBlack++;
        }

        gameState.getGame().performAction(move, false);

        if (depth >= maxDepth || gameState.getGame().isGameEnd()) {
            if (gameState.getGame().movesWithoutMill > 49) return 0;
            return gameState.getGame().color == Color.WHITE ? heuristicWhite.evaluate(gameState) : -heuristicBlack.evaluate(gameState);
        }

        float bestResult = gameState.getGame().color == Color.WHITE ? alpha : beta;

        List<String> possibleMoves = gameState.getGame().getPossibleMoves(gameState.getGame().color);

        if (sortAll) sortMoves(possibleMoves, gameState, gameState.getGame().color == Color.WHITE);

        for (String nextMove : possibleMoves) {
            float result = alphabeta(nextMove, depth + 1, gameState.copy(), alpha, beta);

            if (gameState.getGame().color == Color.WHITE) {
                if (result > bestResult) {
                    bestResult = result;
                    alpha = result;
                }
            } else {
                if (result < bestResult) {
                    bestResult = result;
                    beta = result;
                }
            }

            if (alpha >= beta) return bestResult;

        }
        return bestResult;
    }

    public void sortMoves(List<String> possibleMoves, GameState gameState, boolean descending) {
        List<Double> values = new ArrayList<>();
        for (String move : possibleMoves) {
            GameState gs = gameState.copy();
            gs.getGame().performAction(move, false);
            values.add((double) (gs.getGame().color == Color.WHITE ? heuristicWhite.evaluate(gs) : heuristicBlack.evaluate(gs)));
        }
        bubbleSort(values, possibleMoves, descending);
    }

    private void bubbleSort(List<Double> values, List<String> moves, boolean descending) {
        for (int i = 0; i < values.size() - 1; i++) {
            for (int j = i + 1; j < values.size(); j++) {
                if (descending) {
                    if (values.get(i) < values.get(j)) {
                        replace(values, i, j);
                        replace(moves, i, j);
                    }
                } else {
                    if (values.get(i) > values.get(j)) {
                        replace(values, i, j);
                        replace(moves, i, j);
                    }
                }
            }
        }
    }

    private <T> void replace(List<T> array, int i1, int i2) {
        T temp = array.get(i1);
        array.set(i1, array.get(i2));
        array.set(i2, temp);
    }

}
