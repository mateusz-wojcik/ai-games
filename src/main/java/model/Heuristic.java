package model;

public abstract class Heuristic {

    public abstract float evaluate(GameState gameState);
}
