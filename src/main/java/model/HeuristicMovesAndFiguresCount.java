package model;

public class HeuristicMovesAndFiguresCount extends Heuristic {

    @Override
    public float evaluate(GameState gameState) {
        return (gameState.getGame().getPossibleMoves(Color.WHITE).size() + gameState.getGame().blackKilled)
                - ((gameState.getGame().getPossibleMoves(Color.BLACK).size()) + gameState.getGame().whiteKilled);
    }
}
