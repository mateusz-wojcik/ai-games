package model;

public class Utils {

    public static Color otherColor(Color color){
        return color == Color.WHITE ? Color.BLACK : Color.WHITE;
    }
}
