package model;

import javafx.scene.layout.AnchorPane;

import java.util.*;

public class MovingManager {

    private static final double MOVE_ERROR_TOLERANCE = 30d;

    private static double layoutX, layoutY, width, height;

    private static final Map<String, Coordinate> moveCoordinates = new HashMap<>();
    private static final Map<String, List<String>> neighbours = new HashMap<>();
    private static final List<HashSet<String>> millConfigurations = new ArrayList<>();
    private static final HashMap<String, String> fields = new HashMap<>();
    private static final HashMap<String, List<String>> cmillConfigurations = new HashMap<>();

    public MovingManager(AnchorPane pane){
        layoutX = pane.getLayoutX();
        layoutY = pane.getLayoutY();
        width = pane.getWidth();
        height = pane.getHeight();
        calculateCoordinates();
        setupNeighbours();
        setupMillConfigurations();
        initFields();
        setupCustomMillConfigurations();
    }

    private void calculateCoordinates(){
        moveCoordinates.put("7a", new Coordinate(layoutX, layoutY));
        moveCoordinates.put("7d", new Coordinate(layoutX + 300, layoutY));
        moveCoordinates.put("7g", new Coordinate(layoutX + 600, layoutY));
        moveCoordinates.put("6b", new Coordinate(layoutX + 85, layoutY + 85));
        moveCoordinates.put("6d", new Coordinate(layoutX + 300, layoutY + 85));
        moveCoordinates.put("6f", new Coordinate(layoutX + 515, layoutY + 85));
        moveCoordinates.put("5c", new Coordinate(layoutX + 170, layoutY + 170));
        moveCoordinates.put("5d", new Coordinate(layoutX + 300, layoutY + 170));
        moveCoordinates.put("5e", new Coordinate(layoutX + 430, layoutY + 170));
        moveCoordinates.put("4a", new Coordinate(layoutX, layoutY + 300));
        moveCoordinates.put("4b", new Coordinate(layoutX + 85, layoutY + 300));
        moveCoordinates.put("4c", new Coordinate(layoutX + 170, layoutY + 300));
        moveCoordinates.put("4e", new Coordinate(layoutX + 430, layoutY + 300));
        moveCoordinates.put("4f", new Coordinate(layoutX + 515, layoutY + 300));
        moveCoordinates.put("4g", new Coordinate(layoutX + 600, layoutY + 300));
        moveCoordinates.put("3c", new Coordinate(layoutX + 170, layoutY + 430));
        moveCoordinates.put("3d", new Coordinate(layoutX + 300, layoutY + 430));
        moveCoordinates.put("3e", new Coordinate(layoutX + 430, layoutY + 430));
        moveCoordinates.put("2b", new Coordinate(layoutX + 85, layoutY + 515));
        moveCoordinates.put("2d", new Coordinate(layoutX + 300, layoutY + 515));
        moveCoordinates.put("2f", new Coordinate(layoutX + 515, layoutY + 515));
        moveCoordinates.put("1a", new Coordinate(layoutX, layoutY + 600));
        moveCoordinates.put("1d", new Coordinate(layoutX + 300, layoutY + 600));
        moveCoordinates.put("1g", new Coordinate(layoutX + 600, layoutY + 600));
    }

    private void setupNeighbours(){
        neighbours.put("7a", List.of("7d", "4a"));
        neighbours.put("7d", List.of("7a", "6d", "7g"));
        neighbours.put("7g", List.of("7d", "4g"));
        neighbours.put("6b", List.of("4b", "6d"));
        neighbours.put("6d", List.of("7d", "5d", "6b", "6f"));
        neighbours.put("6f", List.of("6d", "4f"));
        neighbours.put("5c", List.of("5d", "4c"));
        neighbours.put("5d", List.of("6d", "5c", "5e"));
        neighbours.put("5e", List.of("5d", "4e"));
        neighbours.put("4a", List.of("4b", "7a", "1a"));
        neighbours.put("4b", List.of("4a", "4c", "6b", "2b"));
        neighbours.put("4c", List.of("4b", "5c", "3c"));
        neighbours.put("4e", List.of("5e", "3e", "4f"));
        neighbours.put("4f", List.of("6f", "2f", "4e", "4g"));
        neighbours.put("4g", List.of("4f", "7g", "1g"));
        neighbours.put("3c", List.of("3d", "4c"));
        neighbours.put("3d", List.of("2d", "3c", "3e"));
        neighbours.put("3e", List.of("3d", "4e"));
        neighbours.put("2b", List.of("4b", "2d"));
        neighbours.put("2d", List.of("1d", "3d", "2b", "2f"));
        neighbours.put("2f", List.of("2d", "4f"));
        neighbours.put("1a", List.of("4a", "1d"));
        neighbours.put("1d", List.of("1a", "1g", "2d"));
        neighbours.put("1g", List.of("1d", "4g"));
    }

    private void setupMillConfigurations() {
        millConfigurations.add(new HashSet<>(Arrays.asList("1a", "4a", "7a")));
        millConfigurations.add(new HashSet<>(Arrays.asList("2b", "4b", "6b")));
        millConfigurations.add(new HashSet<>(Arrays.asList("3c", "4c", "5c")));
        millConfigurations.add(new HashSet<>(Arrays.asList("1d", "2d", "3d")));
        millConfigurations.add(new HashSet<>(Arrays.asList("5d", "6d", "7d")));
        millConfigurations.add(new HashSet<>(Arrays.asList("3e", "4e", "5e")));
        millConfigurations.add(new HashSet<>(Arrays.asList("2f", "4f", "6f")));
        millConfigurations.add(new HashSet<>(Arrays.asList("1g", "4g", "7g")));
        millConfigurations.add(new HashSet<>(Arrays.asList("1a", "1d", "1g")));
        millConfigurations.add(new HashSet<>(Arrays.asList("2b", "2d", "2f")));
        millConfigurations.add(new HashSet<>(Arrays.asList("3c", "3d", "3e")));
        millConfigurations.add(new HashSet<>(Arrays.asList("4a", "4b", "4c")));
        millConfigurations.add(new HashSet<>(Arrays.asList("4e", "4f", "4g")));
        millConfigurations.add(new HashSet<>(Arrays.asList("5c", "5d", "5e")));
        millConfigurations.add(new HashSet<>(Arrays.asList("6b", "6d", "6f")));
        millConfigurations.add(new HashSet<>(Arrays.asList("7a", "7d", "7g")));
    }

    private void setupCustomMillConfigurations() {
        cmillConfigurations.put("7a", List.of("7a-7d-7g", "7a-4a-1a"));
        cmillConfigurations.put("7d", List.of("7a-7d-7g", "7d-6d-5d"));
        cmillConfigurations.put("7g", List.of("7a-7d-7g", "7g-4g-1g"));
        cmillConfigurations.put("6b", List.of("6b-6d-6f", "6b-4b-2b"));
        cmillConfigurations.put("6d", List.of("6b-6d-6f", "7d-6d-5d"));
        cmillConfigurations.put("6f", List.of("6b-6d-6f", "6f-4f-2f"));
        cmillConfigurations.put("5c", List.of("5c-5d-5e", "5c-4c-3d"));
        cmillConfigurations.put("5d", List.of("5c-5d-5e", "7d-6d-5d"));
        cmillConfigurations.put("5e", List.of("5c-5d-5e", "5e-4e-3e"));
        cmillConfigurations.put("4a", List.of("4a-4b-4c", "7a-4a-1a"));
        cmillConfigurations.put("4b", List.of("4a-4b-4c", "6b-4b-2b"));
        cmillConfigurations.put("4c", List.of("4a-4b-4c", "5c-4c-3c"));
        cmillConfigurations.put("4e", List.of("4e-4f-4g", "5e-4e-3e"));
        cmillConfigurations.put("4f", List.of("4e-4f-4g", "6f-4f-2f"));
        cmillConfigurations.put("4g", List.of("4e-4f-4g", "7g-4g-1g"));
        cmillConfigurations.put("3c", List.of("3c-3d-3e", "5c-4c-3c"));
        cmillConfigurations.put("3d", List.of("3c-3d-3e", "3d-2d-1d"));
        cmillConfigurations.put("3e", List.of("3c-3d-3e", "5e-4e-3e"));
        cmillConfigurations.put("2b", List.of("2b-2d-2f", "6b-4b-2b"));
        cmillConfigurations.put("2d", List.of("2b-2d-2f", "3d-2d-1d"));
        cmillConfigurations.put("2f", List.of("2b-2d-2f", "6f-4f-2f"));
        cmillConfigurations.put("1a", List.of("1a-1d-1g", "7a-4a-1a"));
        cmillConfigurations.put("1d", List.of("1a-1d-1g", "3d-2d-1d"));
        cmillConfigurations.put("1g", List.of("1a-1d-1g", "7g-4g-1g"));
    }

    private void initFields(){
        fields.put("7a", null);
        fields.put("7d", null);
        fields.put("7g", null);
        fields.put("6b", null);
        fields.put("6d", null);
        fields.put("6f", null);
        fields.put("5c", null);
        fields.put("5d", null);
        fields.put("5e", null);
        fields.put("4a", null);
        fields.put("4b", null);
        fields.put("4c", null);
        fields.put("4e", null);
        fields.put("4f", null);
        fields.put("4g", null);
        fields.put("3c", null);
        fields.put("3d", null);
        fields.put("3e", null);
        fields.put("2b", null);
        fields.put("2d", null);
        fields.put("2f", null);
        fields.put("1a", null);
        fields.put("1d", null);
        fields.put("1g", null);
    }

    public static boolean isOnBoard(Figure figure){
        if (figure.getPositionX() >= layoutX - MOVE_ERROR_TOLERANCE
                && figure.getPositionY() >= layoutY - MOVE_ERROR_TOLERANCE
                && figure.getPositionX() <= layoutX + width + MOVE_ERROR_TOLERANCE
                && figure.getPositionY() <= layoutY + height + MOVE_ERROR_TOLERANCE
        ){
            //System.out.println("Figure " + figure.getCircle().getId() + " is on board");
            return true;
        }
        //System.out.println("Figure " + figure.getCircle().getId() + " is not on board");
        return false;
    }


    public static Coordinate getColsestCoordinate(Coordinate coordinate){
        Comparator<Coordinate> coordinateComparator =
                Comparator.comparingDouble(coord -> coord.distanceTo(coordinate));
        //moveCoordinates.values().stream().forEach(coordinate1 -> System.out.println(coordinate1.toString()));
        //System.out.println("MIN " + moveCoordinates.values().stream().min(coordinateComparator).get().toString());
        return moveCoordinates.values().stream().min(coordinateComparator).get();
    }

    public static String getFieldKeyFromCoordinate(Coordinate coordinate){
        for (String key: moveCoordinates.keySet()){
            if (moveCoordinates.get(key).equals(coordinate)) return key;
        }
        return null;
    }

    public static Coordinate getCoordinateFromFieldKey(String fieldKey){
        return moveCoordinates.get(fieldKey);
    }

    public Map<String, Coordinate> getMoveCoordinates(){
        return moveCoordinates;
    }

    public Map<String, List<String>> getNeighbours(){
        return neighbours;
    }

    public static List<HashSet<String>> getMillConfigurations() {
        return millConfigurations;
    }

    public double getLayoutX(){
        return layoutX;
    }

    public double getLayoutY() {
        return layoutY;
    }

    public static HashMap<String, String> getFields() {
        return fields;
    }

    public static HashMap<String, List<String>> getCmillConfigurations() {
        return cmillConfigurations;
    }
}
