package model;

import javafx.scene.shape.Circle;

import java.io.Serializable;

public class Figure implements Serializable {

    private double startDragX, startDragSceneX;
    private double startDragY, startDragSceneY;

    private Circle circle;
    private Color color;
    private String currentPosition;

    private boolean enabled;
    private boolean isDead;
    private boolean interfaceEnabled;

    public Figure(Circle circle){
        this.circle = circle;
        this.color = circle.getId().substring(0, circle.getId().length() - 1).equals("white") ? Color.WHITE: Color.BLACK;
        this.currentPosition = "START";
    }

    public Figure(Figure figure) {
        this.startDragX = figure.startDragX;
        this.startDragY = figure.startDragY;
        this.startDragSceneX = figure.startDragSceneX;
        this.startDragSceneY = figure.startDragSceneY;
        this.circle = new MyCircle(figure.getCircle()); //uwaga
        this.color = figure.color;
        this.currentPosition = figure.currentPosition;
        this.enabled = figure.enabled;
        this.isDead = figure.isDead;
        this.interfaceEnabled = figure.interfaceEnabled;
    }

    public Circle getCircle() {
        return circle;
    }

    public void enableDragBehavior(){
        circle.setOnMousePressed(e -> {
            startDragSceneX = e.getSceneX();
            startDragSceneY = e.getSceneY();
            startDragX = circle.getTranslateX();
            startDragY = circle.getTranslateY();
        });

        circle.setOnMouseDragged(e -> {
            circle.setTranslateX(startDragX + e.getSceneX() - startDragSceneX);
            circle.setTranslateY(startDragY + e.getSceneY() - startDragSceneY);
        });
    }

    public void moveTo(Coordinate position){
        if (interfaceEnabled) {
            circle.setTranslateX(circle.getTranslateX() + position.getX() - getPositionX());
            circle.setTranslateY(circle.getTranslateY() + position.getY() - getPositionY());
        }
        currentPosition = MovingManager.getFieldKeyFromCoordinate(position);
        //System.out.println("Figure " + this.getCircle().getId() + " is on " + currentPosition + " " + getCoordinate().toString());
    }

    public void moveTo(String fieldKey){
        if (interfaceEnabled) {
            circle.setTranslateX(circle.getTranslateX() + MovingManager.getCoordinateFromFieldKey(fieldKey).getX() - getPositionX());
            circle.setTranslateY(circle.getTranslateY() + MovingManager.getCoordinateFromFieldKey(fieldKey).getY() - getPositionY());
        }
        currentPosition = fieldKey;
        //System.out.println("Figure " + this.getCircle().getId() + " is on " + currentPosition + " " + getCoordinate().toString());
    }

    public void kill(){
        this.enabled = false;
        this.isDead = true;
        currentPosition = "END";
        //System.out.println("Kill" + circle.getId());
        if (interfaceEnabled) {
            circle.setTranslateX(0);
            circle.setTranslateY(0);
            circle.setVisible(false);
        }
    }

    public void disableDragBehavior(){
        circle.setOnMousePressed(null);
        circle.setOnMouseDragged(null);
    }

    public void disableMove(){
        disableDragBehavior();
        this.enabled = false;
    }

    public void enableMove(){
        enableDragBehavior();
        this.enabled = true;
    }

    public void reset(){
        this.enabled = false;
        this.isDead = false;
        this.currentPosition = "START";
        if (interfaceEnabled) {
            circle.setVisible(true);
            circle.setTranslateX(0);
            circle.setTranslateY(0);
        }
    }

    public void backToDragStart(){
        if (interfaceEnabled) {
            circle.setTranslateX(startDragX);
            circle.setTranslateY(startDragY);
        }
    }

    public Coordinate getCoordinate(){
        return new Coordinate(getPositionX(), getPositionY());
    }

    /*returns visible x position on screen*/
    public double getPositionX(){
        return circle.getLayoutX() + circle.getTranslateX();
    }

    /*returns visible y position on screen*/
    public double getPositionY(){
        return circle.getLayoutY() + circle.getTranslateY();
    }

    /*returns figure color*/
    public Color getColor() {
        return this.color;
    }

    /*returns current position for example 7a*/
    public String getCurrentPosition() {
        return currentPosition;
    }

    /*sets current position on board for example 7a*/
    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    /*returns true if game for figure is over false otherwise*/
    public boolean isDead() {
        return isDead;
    }

    /*set true if figure needs to be removed from board*/
    public void setDead(boolean dead) {
        isDead = dead;
    }

    /*returns true if fiugre is active*/
    public boolean isEnabled() {
        return enabled;
    }

    public boolean isInterfaceEnabled() {
        return interfaceEnabled;
    }

    public void setInterfaceEnabled(boolean interfaceEnabled) {
        this.interfaceEnabled = interfaceEnabled;
        if (!interfaceEnabled) circle.setVisible(false);
    }

    @Override
    public String toString() {
        return "[" + getCircle().getId() + "(" + getCurrentPosition() + ")]";
    }
}
