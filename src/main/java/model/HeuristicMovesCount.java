package model;

public class HeuristicMovesCount extends Heuristic {


    @Override
    public float evaluate(GameState gameState) {
        return gameState.getGame().getPossibleMoves(Color.WHITE).size() - gameState.getGame().getPossibleMoves(Color.BLACK).size();
    }
}
