package model;

public class HeuristicFiguresCount extends Heuristic {

    @Override
    public float evaluate(GameState gameState) {
        return (9 - gameState.getGame().whiteKilled) - (9 - gameState.getGame().blackKilled);
    }
}
