package model;

public class Coordinate {
    private double x;
    private double y;

    public Coordinate(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanceTo(Coordinate coordinate){
        return Math.sqrt(Math.pow(coordinate.getX() - this.x, 2) + Math.pow(coordinate.getY() - this.y, 2));
    }

    @Override
    public String toString() {
        return "x: " + x + " y:" + y;
    }
}
